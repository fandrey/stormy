package org.stormy;

import android.Manifest;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.PackageManager;
import android.system.Os;
import android.view.WindowManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.libsdl.app.SDLActivity;

public class StormyActivity extends SDLActivity
{
    private static final int REQUESTCODE_PERMISSION_STORAGE = 1234;

    public static final String LOG_TAG = "stormy";
    public String prefix;
    public String bootstrap;

    @Override
    protected String[] getLibraries() {
        return new String[] {
            "SDL2",
            "ffi",
            "guile-2.2",
            "python2.7",
            "crypto",
            "ssl",
            "z",
            "bz2",
            "stormy"
        };
    }

    @Override
    protected String[] getArguments() {
        return new String[] {
            getApplicationInfo().nativeLibraryDir,
            getFilesDir().getAbsolutePath(),
            getExternalFilesDir(null).getAbsolutePath()
        };
    }

    /** For processes to access shared internal storage (/sdcard) we need this permission. */
    @TargetApi(Build.VERSION_CODES.M)
    public boolean ensureStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                   REQUESTCODE_PERMISSION_STORAGE);
                return false;
            }
        } else {
            // Always granted before Android 6.0.
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefix = getFilesDir().getAbsolutePath() + "/usr";
        bootstrap = getExternalFilesDir(null).getAbsolutePath() + "/stormy.zip";

        setupIfNeeded(new Runnable() {
                @Override
                public void run() {
                    // Do something
                }
                });
    }

    void setupIfNeeded(final Runnable whenDone) {
        // Stormy can only be run as the primary user (device owner)
        // since only that account has the expected file system
        // paths. Verify that:
        UserManager um = (UserManager) getSystemService(Context.USER_SERVICE);
        boolean isPrimaryUser = um.getSerialNumberForUser(android.os.Process.myUserHandle()) == 0;
        if (!isPrimaryUser) {
            new AlertDialog.Builder(this)
                .setTitle(R.string.bootstrap_error_title)
                .setMessage(R.string.bootstrap_error_not_primary_user_message)
                .setOnDismissListener(new OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            System.exit(0);
                        }
                    }).setPositiveButton(android.R.string.ok, null).show();
            return;
        }

        final File prefix_file = new File(prefix);
        if (prefix_file.isDirectory()) {
            whenDone.run();
            return;
        }

        final ProgressDialog progress =
            ProgressDialog.show(this,
                                null,
                                getString(R.string.bootstrap_installing),
                                true,
                                false);
        new Thread() {
            @Override
            public void run() {
                try {
                    final String staging_prefix = StormyActivity.this.prefix + "-staging";
                    final File staging_prefix_file = new File(staging_prefix);

                    if (staging_prefix_file.exists()) {
                        deleteFolder(staging_prefix_file);
                    }

                    final byte[] buffer = new byte[8096];
                    final URL zipUrl = new URL("file://" + StormyActivity.this.bootstrap);

                    try (ZipInputStream zipInput = new ZipInputStream(zipUrl.openStream())) {
                        ZipEntry zipEntry;
                        while ((zipEntry = zipInput.getNextEntry()) != null) {
                            String zipEntryName = zipEntry.getName();
                            File targetFile = new File(staging_prefix, zipEntryName);
                            if (zipEntry.isDirectory()) {
                                if (!targetFile.mkdirs())
                                    throw new RuntimeException("Failed to create directory: " +
                                                               targetFile.getAbsolutePath());
                            } else {
                                try (FileOutputStream outStream = new FileOutputStream(targetFile)) {
                                    int readBytes;
                                    while ((readBytes = zipInput.read(buffer)) != -1)
                                        outStream.write(buffer, 0, readBytes);
                                }
                                if (zipEntryName.endsWith(".so")) {
                                    Os.chmod(targetFile.getAbsolutePath(), 0700);
                                }
                            }
                        }
                    }

                    if (!staging_prefix_file.renameTo(prefix_file)) {
                        throw new RuntimeException("Unable to rename staging folder");
                    }

                    StormyActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                whenDone.run();
                            }
                        });
                } catch (final Exception e) {
                    Log.e(StormyActivity.LOG_TAG, "Bootstrap error", e);
                    StormyActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    new AlertDialog.Builder(StormyActivity.this)
                                        .setTitle(R.string.bootstrap_error_title)
                                        .setMessage(R.string.bootstrap_error_body)
                                        .setNegativeButton(R.string.bootstrap_error_abort, new OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    StormyActivity.this.finish();
                                                }
                                            }).setPositiveButton(R.string.bootstrap_error_try_again, new OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        StormyActivity.this.setupIfNeeded(whenDone);
                                                    }
                                                }).show();
                                } catch (WindowManager.BadTokenException e) {
                                    // Activity already dismissed - ignore.
                                }
                            }
                        });
                } finally {
                    StormyActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progress.dismiss();
                                } catch (RuntimeException e) {
                                    // Activity already dismissed - ignore.
                                }
                            }
                        });
                }
            }
        }.start();
    }

    /** Delete a folder and all its content or throw. Don't follow symlinks. */
    static void deleteFolder(File fileOrDirectory) throws IOException {
        if (fileOrDirectory.getCanonicalPath().equals(fileOrDirectory.getAbsolutePath()) &&
            fileOrDirectory.isDirectory()) {
            File[] children = fileOrDirectory.listFiles();

            if (children != null) {
                for (File child : children) {
                    deleteFolder(child);
                }
            }
        }

        if (!fileOrDirectory.delete()) {
            throw new RuntimeException("Unable to delete " +
                                       (fileOrDirectory.isDirectory() ? "directory " : "file ") +
                                       fileOrDirectory.getAbsolutePath());
        }
    }
}
