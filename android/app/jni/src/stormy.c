#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include <SDL.h>
#include <libguile.h>
#include <python2.7/Python.h>

#define LOGI(...) SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, __VA_ARGS__)
/* logcat *:S SDL/APP */

static void * run_guile(void * data)
{
  scm_c_define("%storage", scm_from_latin1_string((char*)data));
  scm_primitive_load_path(scm_from_latin1_string("stormy.scm"));

  return NULL;
}

int main(int argc, char ** argv)
{
  char tmp[16386];
  FILE * f;
  char * python_args[] = {"python2",
                          "-c",
                          "import sys\n"
                          "from allmydata.scripts.runner import run\n"
                          "sys.argv.append('create-client')\n"
                          "run()"};

  sprintf(tmp, "%s/output", argv[3]);
  freopen(tmp, "w", stdout);

  sprintf(tmp, "%s/errors", argv[3]);
  freopen(tmp, "w", stderr);

  sprintf(tmp,
          "%s/usr/share/guile/2.2:"
          "%s/usr/share/guile/site/2.2:"
          "%s/usr/share/guile/site:"
          "%s/usr/share/guile",
          argv[2], argv[2], argv[2], argv[2]);
  setenv("GUILE_SYSTEM_PATH", tmp, 1);

  sprintf(tmp,
          "%s/usr/lib/guile/2.2/ccache:"
          "%s/usr/lib/guile/2.2/site-ccache",
          argv[2], argv[2]);
  setenv("GUILE_SYSTEM_COMPILED_PATH", tmp, 1);

  sprintf(tmp, "%s/.guile:%s/.guile", argv[3], argv[2]);
  setenv("GUILE_LOAD_PATH", tmp, 1);

  sprintf(tmp, "%s/.cache", argv[2]);
  setenv("GUILE_LOAD_COMPILED_PATH", tmp, 1);

  sprintf(tmp, "%s/usr", argv[2]);
  setenv("PYTHONHOME", tmp, 1);

  setenv("HOME", argv[2], 1);

  sprintf(tmp, "%s/tmp", argv[2]);
  setenv("TEMP", tmp, 1);

  printf("uid => %d\n", getuid());
  printf("argv[2] => %s\n", argv[2]);

  /* printf("Py_Main() => %d\n", Py_Main(3, python_args)); */
  /* fflush(stdout); */
  /* fflush(stderr); */

  scm_with_guile(run_guile, argv[3]);
  fflush(stdout);
  fflush(stderr);

  fclose(stdout);
  fclose(stderr);

  return 0;
}
