LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := guile-2.2
LOCAL_SRC_FILES := libguile-2.2.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := python2.7
LOCAL_SRC_FILES := libpython2.7.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := crypto
LOCAL_SRC_FILES := libcrypto.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ssl
LOCAL_SRC_FILES := libssl.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := stormy

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := \
  $(LOCAL_PATH)/$(SDL_PATH)/include \
  $(LOCAL_PATH)/include \
  $(LOCAL_PATH)/include/guile/2.2

LOCAL_SRC_FILES := $(LOCAL_PATH)/stormy.c

LOCAL_SHARED_LIBRARIES := SDL2 guile-2.2 python2.7 crypto ssl

LOCAL_LDLIBS := -ldl -lGLESv2 -llog -landroid

include $(BUILD_SHARED_LIBRARY)
