-include config.mk

assert-defined = $(foreach varname,$(strip $1),\
  $(if $(strip $($(varname))),,\
    $(error $(varname) is not defined)\
  )\
)

$(call assert-defined,ANDROID_SDK_PATH \
                      ANDROID_NDK_PATH \
                      LIBTOOL_PATH \
                      GMP_PATH \
                      LIBUNISTRING_PATH \
                      LIBATOMIC_PATH \
                      GC_PATH \
                      LIBFFI_PATH \
                      LIBICONV_PATH \
                      GUILE_PATH \
                      SDL2_PATH \
                      PYTHON2_PATH \
                      OPENSSL_PATH \
                      ZLIB_PATH \
                      BZIP2_PATH \
                      SQLITE_PATH)

LIBTOOL_NAME = ltdl
GMP_NAME = gmp
LIBUNISTRING_NAME = unistring
GC_NAME = gc
LIBICONV_NAME = iconv
LIBATOMIC_NAME = atomic_ops

include $(ANDROID_NDK_PATH)/build/core/toolchains/aarch64-linux-android-clang/config.mk

# include $(ANDROID_NDK_PATH)/build/core/toolchains/aarch64-linux-android-clang/setup.mk
TOOLCHAIN_NAME := aarch64-linux-android
TOOLCHAIN_ROOT := $(ANDROID_NDK_PATH)/toolchains/$(TOOLCHAIN_NAME)-4.9/prebuilt/linux-x86_64

LLVM_TRIPLE := aarch64-none-linux-android

CLANG_CROSS_FLAGS := \
    --gcc-toolchain=$(TOOLCHAIN_ROOT) \
    -target $(LLVM_TRIPLE) \

TARGET_CFLAGS := \
    -ffunction-sections \
    -funwind-tables \
    -fstack-protector-strong \
    -fpic \
    -Wno-invalid-command-line-argument \
    -Wno-unused-command-line-argument \
    -no-canonical-prefixes \

# Always enable debug info. We strip binaries when needed.
TARGET_CFLAGS += -g

TARGET_LDFLAGS += \
    -no-canonical-prefixes \

TARGET_$(TARGET_ARCH)_release_CFLAGS := \
    -O2 \
    -DNDEBUG \

TARGET_$(TARGET_ARCH)_debug_CFLAGS := \
    -O0 \
    -UNDEBUG \
    -fno-limit-debug-info \

# end include

SYSROOT_ARCH_INC_ARG := \
    -isystem $(ANDROID_NDK_PATH)/sysroot/usr/include \
    -isystem $(ANDROID_NDK_PATH)/sysroot/usr/include/aarch64-linux-android

SYSROOT := --sysroot $(ANDROID_NDK_PATH)/platforms/android-23/arch-$(TOOLCHAIN_ARCH)

GUILE_HOST_PATH := $(CURDIR)/guile-host
INSTALL_ROOT := $(CURDIR)/install-root
INSTALL_PREFIX := $(CURDIR)/install-root/usr
lib-prefix = $(addprefix $(INSTALL_PREFIX)/lib/lib,$(addsuffix $(or $(2),.a),$(1)))

LIBGUILE := $(call lib-prefix,guile-2.2,.so)

STORMY_APK := $(CURDIR)/android/app/build/outputs/apk/app-debug.apk
STORMY_ZIP := $(CURDIR)/stormy.zip

PATH := $(wildcard $(ANDROID_NDK_PATH)/toolchains/llvm/prebuilt/linux-x86_64/bin):$(wildcard $(ANDROID_NDK_PATH)/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin):$(PATH)

CFLAGS = $(TARGET_CFLAGS) $(TARGET_$(TARGET_ARCH)_debug_CFLAGS) -I$(INSTALL_PREFIX)/include
CC = "clang $(CLANG_CROSS_FLAGS) $(SYSROOT) $(SYSROOT_ARCH_INC_ARG)"

define build-rules
$(1)/Makefile: $(3)
	cd $(1) && \
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_ROOT) \
	./configure --host=$(TOOLCHAIN_NAME) \
                    --prefix=$(INSTALL_PREFIX) \
                    $(if $(4),--enable-shared,--disable-shared) \
                    CFLAGS="$(CFLAGS)" \
                    CXXFLAGS="$(CFLAGS)" \
                    LDFLAGS="$(TARGET_LDFLAGS) -L$(INSTALL_PREFIX)/lib" \
                    CC=$(CC) \
                    CXX=$(CC) \
                    $(5)

$(2): $(1)/Makefile
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_ROOT) make -C $(1)
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_ROOT) make -C $(1) install
endef

all: $(STORMY_APK)

clean:
	cd android && ANDROID_HOME=$(ANDROID_SDK_PATH) ANDROID_NDK_HOME=$(ANDROID_NDK_PATH) gradle clean cleanBuildCache

$(INSTALL_ROOT)/lib/.empty:
	mkdir -p $(INSTALL_PREFIX)/lib
	cd $(INSTALL_ROOT) && ln -sf usr/lib lib
	touch $@

$(foreach lib,LIBTOOL GMP LIBUNISTRING LIBICONV LIBATOMIC,$(eval $(call build-rules,$($(lib)_PATH),$(call lib-prefix,$($(lib)_NAME)))))
$(eval $(call build-rules,$(GC_PATH),$(call lib-prefix,$(GC_NAME)),$(INSTALL_ROOT)/lib/.empty $(call lib-prefix,$(LIBATOMIC_NAME))))

LIBFFI := $(call lib-prefix,ffi,.so)
$(eval $(call build-rules,$(LIBFFI_PATH),$(LIBFFI),$(INSTALL_ROOT)/lib/.empty,shared))

GUILE_LIBS := $(foreach lib,LIBTOOL GMP LIBUNISTRING GC LIBICONV,$(call lib-prefix,$($(lib)_NAME))) $(call lib-prefix,ffi,.so)

$(GUILE_PATH)/.patched: $(CURDIR)/guile-2.2-android.patch $(INSTALL_ROOT)/lib/.empty $(GUILE_LIBS) $(GUILE_HOST_PATH)/bin/guile
	cd $(GUILE_PATH) && patch -p1 < $< && aclocal && automake
	touch $@

$(GUILE_HOST_PATH)/bin/guile:
	mkdir -p $(GUILE_PATH)/build-host
	cd $(GUILE_PATH)/build-host && ../configure --prefix=$(GUILE_HOST_PATH)
	make -C $(GUILE_PATH)/build-host
	make -C $(GUILE_PATH)/build-host install

$(eval $(call build-rules,$(GUILE_PATH),$(LIBGUILE),$(GUILE_PATH)/.patched $(foreach lib,$(GUILE_LIBS),$(call lib-prefix,$($(lib)_NAME))),shared,GUILE_FOR_BUILD=$(GUILE_HOST_PATH)/bin/guile))

LIBCRYPTO = $(call lib-prefix,crypto,.so)
LIBSSL = $(call lib-prefix,ssl,.so)

$(OPENSSL_PATH)/Makefile:
	cd $(OPENSSL_PATH) && \
	PATH=$(PATH) ANDROID_NDK=$(ANDROID_NDK_PATH) ./Configure --prefix=$(INSTALL_PREFIX) android-arm64 -D__ANDROID_API__=23

$(LIBCRYPTO) $(LIBSSL): $(OPENSSL_PATH)/Makefile
	cd $(OPENSSL_PATH) && PATH=$(PATH) ANDROID_NDK=$(ANDROID_NDK_PATH) make
	cd $(OPENSSL_PATH) && PATH=$(PATH) ANDROID_NDK=$(ANDROID_NDK_PATH) make install

LIBZ = $(call lib-prefix,z,.so)

$(ZLIB_PATH)/.configured:
	cd $(ZLIB_PATH) && \
	PATH=$(PATH) \
        CFLAGS="$(CFLAGS)" \
        LDFLAGS="$(TARGET_LDFLAGS)" \
        CC=$(CC) \
        ./configure --prefix=$(INSTALL_PREFIX)
	touch $@

$(LIBZ): $(ZLIB_PATH)/.configured
	make -C $(ZLIB_PATH)
	make -C $(ZLIB_PATH) install

LIBBZ2 = $(call lib-prefix,bz2,.so)

AT = @
DOLLAR = $
$(BZIP2_PATH)/Makefile-android: $(BZIP2_PATH)/Makefile-libbz2_so
	sed -e 's$(AT)^CC=.*$(DOLLAR)$(AT)CC='$(CC)'$(AT)' \
            -e 's$(AT)^CFLAGS=\(.*\)$(DOLLAR)$(AT)CFLAGS=\1 $(CFLAGS)$(AT)' \
            -e 's$(AT)^\(.*-o libbz2\.so.*\)$(DOLLAR)$(AT)\1 $(TARGET_LDFLAGS)$(AT)' $^ > $@

$(LIBBZ2): $(BZIP2_PATH)/Makefile-android
	make -C $(BZIP2_PATH) -f $^
	cd $(BZIP2_PATH) && ln -sf `find -name libbz2.so.\* -type f -printf %f` libbz2.so
	cp -a $(BZIP2_PATH)/bzlib.h $(INSTALL_PREFIX)/include
	cp -a $(BZIP2_PATH)/libbz2.so* $(INSTALL_PREFIX)/lib

LIBSQLITE = $(call lib-prefix,sqlite3,.so)

$(SQLITE_PATH)/Makefile:
	cd $(SQLITE_PATH) && \
        PATH=$(PATH) \
        ./configure --host=$(TOOLCHAIN_NAME) \
		    --prefix=$(INSTALL_PREFIX) \
		    --disable-static \
		    --enable-fts3 \
		    --enable-fts4 \
		    --enable-fts5 \
		    --enable-rtree \
		    --enable-json1 \
		    CFLAGS="$(CFLAGS)" \
		    CPPFLAGS="$(CFLAGS) -DSQLITE_ENABLE_COLUMN_METADATA=1 \
					-DSQLITE_ENABLE_UNLOCK_NOTIFY \
					-DSQLITE_ENABLE_DBSTAT_VTAB=1 \
					-DSQLITE_ENABLE_FTS3_TOKENIZER=1 \
					-DSQLITE_SECURE_DELETE \
					-DSQLITE_MAX_VARIABLE_NUMBER=250000 \
					-DSQLITE_MAX_EXPR_DEPTH=10000" \
                    LDFLAGS="$(TARGET_LDFLAGS) -L$(INSTALL_PREFIX)/lib" \
                    CC=$(CC)

$(LIBSQLITE): $(SQLITE_PATH)/Makefile
	PATH=$(PATH) make -C $(SQLITE_PATH)
	PATH=$(PATH) make -C $(SQLITE_PATH) install

PYTHON2_CONFIGURE_VARS = ac_cv_file__dev_ptmx=yes \
                         ac_cv_file__dev_ptc=no \
                         ac_cv_func_wcsftime=no \
                         ac_cv_func_ftime=no \
                         ac_cv_func_faccessat=no \
                         ac_cv_func_gethostbyname_r=no \
                         ac_cv_buggy_getaddrinfo=no \
                         ac_cv_little_endian_double=yes

PYTHON2_PATCHES = python2-bltinmodule.c.patch \
                  python2-configure.patch \
                  python2-fix-dlfcn.patch64 \
                  python2-Makefile.pre.in.patch \
                  python2-Modules-pwdmodule.c.patch \
                  python2-setup.py.patch

LIBPYTHON = $(call lib-prefix,python2.7,.so)

define python2-patch
cd $(PYTHON2_PATH) && patch -p1 < $(CURDIR)/$(pat)

endef

$(PYTHON2_PATH)/.patched: $(PYTHON2_PATCHES)
	$(foreach pat,$(PYTHON2_PATCHES),$(python2-patch))
	touch $@

$(PYTHON2_PATH)/Makefile: $(PYTHON2_PATH)/.patched $(LIBCRYPTO) $(LIBSSL) $(LIBZ) $(LIBBZ2) $(LIBSQLITE)
	cd $(PYTHON2_PATH) && \
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_ROOT) \
	./configure --host=$(TOOLCHAIN_NAME) \
                    --build=x86_64-pc-linux-gnu \
                    --prefix=$(INSTALL_PREFIX) \
                    --enable-shared \
                    --with-threads \
                    --enable-unicode=ucs4 \
                    --with-system-ffi \
                    --without-ensurepip \
                    $(PYTHON2_CONFIGURE_VARS) \
                    CFLAGS="$(CFLAGS) -fPIE" \
                    CXXFLAGS="$(CFLAGS) -fPIE" \
                    CPPFLAGS="$(CFLAGS)" \
                    LDFLAGS="$(TARGET_LDFLAGS) -L$(INSTALL_PREFIX)/lib -pie" \
                    CC=$(CC) \
                    CXX=$(CC)

$(LIBPYTHON): $(PYTHON2_PATH)/Makefile
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_PREFIX) make -C $(PYTHON2_PATH)
	PATH=$(PATH) PKG_CONFIG_SYSROOT_DIR=$(INSTALL_PREFIX) make -C $(PYTHON2_PATH) install

LIBCXX_PATH := $(ANDROID_NDK_PATH)/sources/cxx-stl/llvm-libc++/libs/arm64-v8a
LIBCXX := $(LIBCXX_PATH)/libc++_shared.so

$(STORMY_APK): $(LIBGUILE) $(LIBPYTHON) $(LIBCRYPTO) $(LIBSSL) $(LIBZ) $(CURDIR)/android/app/jni/src/stormy.c $(CURDIR)/android/app/src/main/java/org/stormy/StormyActivity.java
	mkdir -p android/app/jni/SDL
	ln -fst android/app/jni/SDL $(SDL2_PATH)/include $(SDL2_PATH)/src
	ln -fs $(INSTALL_PREFIX)/include $(LIBGUILE) $(LIBPYTHON) $(LIBCRYPTO) $(LIBSSL) android/app/jni/src
	mkdir -p android/app/libs/arm64-v8a
	ln -fs $(LIBFFI) $(LIBZ) $(LIBBZ2) $(LIBCXX) android/app/libs/arm64-v8a
	cd android && ANDROID_HOME=$(ANDROID_SDK_PATH) ANDROID_NDK_HOME=$(ANDROID_NDK_PATH) gradle assembleDebug

$(STORMY_ZIP):
	rm -rf $(CURDIR)/stormy-zip
	mkdir -p $(CURDIR)/stormy-zip/{lib,share}
	cp -a $(INSTALL_PREFIX)/lib/guile $(INSTALL_PREFIX)/lib/python2.7 $(CURDIR)/stormy-zip/lib
	cp -a $(INSTALL_PREFIX)/share/guile $(CURDIR)/stormy-zip/share
	7z a -tzip -mx9 $@ $(CURDIR)/stormy-zip/*

VENV = $(CURDIR)/venv
MKVENV := virtualenv2 $(VENV) >/dev/null 2>&1 && echo $(INSTALL_PREFIX) > $(VENV)/lib/python2.7/orig-prefix.txt

PAREN = )
CRYPTOGRAPHY_VERSION = $(shell rm -rf $(VENV); $(MKVENV) && cd $(VENV) && bin/pip search cryptography | sed -n 's/^cryptography (\([^$(PAREN)]*\)).*$$/\1/p')
CRYPTOGRAPHY_WHEEL = $(CURDIR)/wheels/cryptography-$(CRYPTOGRAPHY_VERSION)-cp27-cp27mu-android_aarch64.whl
PYNACL_VERSION = $(shell rm -rf $(VENV); $(MKVENV) && cd $(VENV) && bin/pip search pynacl | sed -n 's/^PyNaCl (\([^$(PAREN)]*\)).*$$/\1/p')
PYNACL_WHEEL = $(CURDIR)/wheels/PyNaCl-$(PYNACL_VERSION)-cp27-cp27mu-android_aarch64.whl
PYCRYPTOPP_VERSION = $(shell rm -rf $(VENV); $(MKVENV) && cd $(VENV) && bin/pip search pycryptopp | sed -n 's/^pycryptopp (\([^$(PAREN)]*\)).*$$/\1/p')
PYCRYPTOPP_WHEEL = $(CURDIR)/wheels/pycryptopp-$(PYCRYPTOPP_VERSION)-cp27-cp27mu-android_aarch64.whl

PIPENV = \
    HOME=`pwd` \
    CFLAGS="$(CFLAGS) -I$(ANDROID_NDK_PATH)/sources/cxx-stl/llvm-libc++/include" \
    LDFLAGS="$(TARGET_LDFLAGS)" \
    CC=$(CC) \
    PATH="$(PATH)" \
    PKG_CONFIG_SYSROOT_DIR=$(INSTALL_PREFIX) \
    _PYTHON_HOST_PLATFORM=android-aarch64

$(CRYPTOGRAPHY_WHEEL):
	mkdir -p $(CURDIR)/wheels
	rm -rf $(VENV)
	$(MKVENV)
	cd $(VENV) && \
	bin/pip install cffi && \
	$(PIPENV) bin/pip install --no-build-isolation cryptography
	find $(VENV)/.cache/pip/wheels -name cryptography-*-android_aarch64.whl -exec cp -a {} $(CURDIR)/wheels \;

$(PYNACL_WHEEL):
	mkdir -p $(CURDIR)/wheels 
	rm -rf $(VENV)
	$(MKVENV)
	cd $(VENV) && \
	bin/pip install cffi && \
	$(PIPENV) bin/pip download --no-build-isolation pynacl && \
	tar xf PyNaCl-$(PYNACL_VERSION).tar.*
	cd $(VENV)/PyNaCl-$(PYNACL_VERSION) && patch -p1 < $(CURDIR)/pynacl.patch
	cd $(VENV) && \
	$(PIPENV) bin/pip install --no-build-isolation $(VENV)/PyNaCl-$(PYNACL_VERSION)
	find $(VENV)/.cache/pip/wheels -name PyNaCl-*-android_aarch64.whl -exec cp -a {} $(CURDIR)/wheels \;

$(PYCRYPTOPP_WHEEL):
	mkdir -p $(CURDIR)/wheels 
	rm -rf $(VENV)
	$(MKVENV)
	cd $(VENV) && \
	$(PIPENV) bin/pip download --no-build-isolation pycryptopp && \
	tar xf pycryptopp-$(PYCRYPTOPP_VERSION).tar.*
	sed -i "s@extra_link_args=\[\]@extra_link_args=['-L$(LIBCXX_PATH)', '-lc++_shared']@" $(VENV)/pycryptopp-$(PYCRYPTOPP_VERSION)/setup.py
	cd $(VENV) && \
	$(PIPENV) bin/pip install --no-build-isolation $(VENV)/pycryptopp-$(PYCRYPTOPP_VERSION)
	find $(VENV)/.cache/pip/wheels -name pycryptopp-*-android_aarch64.whl -exec cp -a {} $(CURDIR)/wheels \;

$(INSTALL_PREFIX)/lib/python2.7/site-packages/allmydata: $(CRYPTOGRAPHY_WHEEL) $(PYNACL_WHEEL) $(PYCRYPTOPP_WHEEL)
	rm -rf $(VENV)
	$(MKVENV)
	cd $(VENV) && \
	$(PIPENV) bin/pip install $^ tahoe-lafs
	cp -a $(VENV)/lib/python2.7/site-packages $(INSTALL_PREFIX)/lib/python2.7
